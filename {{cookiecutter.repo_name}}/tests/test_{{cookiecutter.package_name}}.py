"""Package level tests"""

from {{cookiecutter.package_name}} import __version__


def test_version() -> None:
    """Make sure version matches expected"""
    assert __version__ == "{{ cookiecutter.version }}"

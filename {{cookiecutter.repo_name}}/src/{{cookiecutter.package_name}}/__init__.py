""" {{ cookiecutter.project_short_description }} """

__version__ = "{{ cookiecutter.version }}"  # NOTE Use `bump2version --config-file patch` to bump versions correctly
